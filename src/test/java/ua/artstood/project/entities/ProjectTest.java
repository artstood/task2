package ua.artstood.project.entities;

import org.junit.Before;
import org.junit.Test;
import ua.artstood.project.entities.builders.EmployeeBuilder;
import ua.artstood.project.entities.builders.ProjectBuilder;
import ua.artstood.project.entities.enums.DeveloperLevel;
import ua.artstood.project.entities.enums.EnglishLevel;

import java.sql.*;
import java.util.*;
import java.util.Date;

import static org.junit.Assert.*;

public class ProjectTest {
    private Employee expectedEmployee;
    private Project expectedProject;
    private Feedback expectedFeedback;
    private Team expectedTeam;

    @Before
    public void init(){
        expectedFeedback = new Feedback(
                0,
                "Feedback test",
                new GregorianCalendar(2021, Calendar.MAY, 28).getTime()
        );

        expectedEmployee = new EmployeeBuilder()
                .setId(0)
                .setFirstName("Alex")
                .setLastName("Ivanov")
                .setPatronymic("Victorovich")
                .setEmployeeAcronym("AIV")
                .setEmail("aiv@gmail.com")
                .setPhoneNumber("(097)765-54-32")
                .setBirthdayDate(new GregorianCalendar(1998, Calendar.OCTOBER, 2).getTime())
                .setExperience(1.5f)
                .setEmploymentDate(new GregorianCalendar(2021, Calendar.FEBRUARY, 20).getTime())
                .setProject(expectedProject)
                .setDeveloperLevel(DeveloperLevel.J1)
                .setEnglishLevel(EnglishLevel.B2)
                .setSkype("aivSkype")
                .setFeedback( expectedFeedback)
                .build();
        expectedProject = new ProjectBuilder()
                .setProjectId(0)
                .setProjectName("Test project")
                .setClient("Ivan")
                .setDeadLine(new GregorianCalendar(2021, Calendar.MAY, 30).getTime())
                .setMethodology("Scrum")
                .setProjectManager("Andrew")
                .setTeam(expectedTeam)
                .build();

        expectedEmployee.setProject(expectedProject);

        expectedTeam = new Team(
                0,
                new ArrayList<Employee>()
        );
        expectedTeam.getEmployees().add(expectedEmployee);

        expectedProject.setTeam(expectedTeam);
    }
}