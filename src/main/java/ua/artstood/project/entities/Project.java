package ua.artstood.project.entities;

import java.util.Date;

public class Project {
    private long projectId;
    private String projectName;
    private String client;
    private Date deadLine;
    private String methodology;
    private String projectManager;
    private Team team;

    public Project() {
    }

    public Project(long projectId, String projectName, String client, Date deadLine, String methodology, String projectManager, Team team) {
        this.projectId = projectId;
        this.projectName = projectName;
        this.client = client;
        this.deadLine = deadLine;
        this.methodology = methodology;
        this.projectManager = projectManager;
        this.team = team;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public Date getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(Date deadLine) {
        this.deadLine = deadLine;
    }

    public String getMethodology() {
        return methodology;
    }

    public void setMethodology(String methodology) {
        this.methodology = methodology;
    }

    public String getProjectManager() {
        return projectManager;
    }

    public void setProjectManager(String projectManager) {
        this.projectManager = projectManager;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @Override
    public String toString() {
        return "Project{" +
                "projectId=" + projectId +
                ", projectName='" + projectName + '\'' +
                ", client='" + client + '\'' +
                ", deadLine=" + deadLine +
                ", methodology='" + methodology + '\'' +
                ", projectManager='" + projectManager + '\'' +
                ", team=" + team +
                '}';
    }
}
