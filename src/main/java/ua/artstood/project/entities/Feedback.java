package ua.artstood.project.entities;

import java.util.Date;

public class Feedback {
    private long feedbackId;
    private String description;
    private Date date;

    public Feedback() {
    }

    public Feedback(long feedbackId, String description, Date date) {
        this.feedbackId = feedbackId;
        this.description = description;
        this.date = date;
    }

    public long getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(long feedbackId) {
        this.feedbackId = feedbackId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "feedbackId=" + feedbackId +
                ", description='" + description + '\'' +
                ", date=" + date.toString() +
                '}';
    }
}
