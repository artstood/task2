package ua.artstood.project.entities.enums;

public enum EnglishLevel {
    B1,
    B2,
    C1,
    C2
}
