package ua.artstood.project.entities.builders;

import ua.artstood.project.entities.Project;
import ua.artstood.project.entities.Team;

import java.util.Date;

public class ProjectBuilder {
    private Project project;

    public ProjectBuilder() {
        project = new Project();
    }

    public ProjectBuilder setProjectId(long projectId) {
        this.project.setProjectId(projectId);
        return this;
    }

    public ProjectBuilder setProjectName(String projectName) {
        this.project.setProjectName(projectName);
        return this;
    }

    public ProjectBuilder setClient(String client) {
        this.project.setClient(client);
        return this;
    }

    public ProjectBuilder setDeadLine(Date deadline) {
        this.project.setDeadLine(deadline);
        return this;
    }

    public ProjectBuilder setMethodology(String methodology) {
        this.project.setMethodology(methodology);
        return this;
    }

    public ProjectBuilder setProjectManager(String projectManager) {
        this.project.setProjectManager(projectManager);
        return this;
    }

    public ProjectBuilder setTeam(Team team) {
        this.project.setTeam(team);
        return this;
    }

    public Project build() {
        return project;
    }

}
