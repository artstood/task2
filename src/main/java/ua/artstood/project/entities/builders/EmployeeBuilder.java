package ua.artstood.project.entities.builders;

import ua.artstood.project.entities.Employee;
import ua.artstood.project.entities.Feedback;
import ua.artstood.project.entities.Project;
import ua.artstood.project.entities.enums.DeveloperLevel;
import ua.artstood.project.entities.enums.EnglishLevel;

import java.util.Date;

public class EmployeeBuilder {
    private Employee employee;

    public EmployeeBuilder() {
        employee = new Employee();
    }

    public EmployeeBuilder setId(long id){
        employee.setEmployeeId(id);
        return this;
    }

    public EmployeeBuilder setFirstName(String firstName) {
        employee.setFirstName(firstName);
        return this;
    }

    public EmployeeBuilder setLastName(String lastName) {
        employee.setLastName(lastName);
        return this;
    }

    public EmployeeBuilder setPatronymic(String patronymic) {
        employee.setPatronymic(patronymic);
        return this;
    }

    public EmployeeBuilder setEmployeeAcronym(String acronym) {
        employee.setEmployeeAcronym(acronym);
        return this;
    }

    public EmployeeBuilder setEmail(String email) {
        employee.setEmail(email);
        return this;
    }

    public EmployeeBuilder setPhoneNumber(String phoneNumber) {
        employee.setPhoneNumber(phoneNumber);
        return this;
    }

    public EmployeeBuilder setBirthdayDate(Date birthday) {
        employee.setBirthdayDate(birthday);
        return this;
    }

    public EmployeeBuilder setExperience(float experience) {
        employee.setExperience(experience);
        return this;
    }

    public EmployeeBuilder setEmploymentDate(Date employmentDate) {
        employee.setEmploymentDate(employmentDate);
        return this;
    }

    public EmployeeBuilder setProject(Project project) {
        employee.setProject(project);
        return this;
    }

    public EmployeeBuilder setDeveloperLevel(DeveloperLevel level) {
        employee.setDeveloperLevel(level);
        return this;
    }

    public EmployeeBuilder setEnglishLevel(EnglishLevel level) {
        employee.setEnglishLevel(level);
        return this;
    }

    public EmployeeBuilder setSkype(String skype) {
        employee.setSkype(skype);
        return this;
    }

    public EmployeeBuilder setFeedback(Feedback feedback) {
        employee.setFeedback(feedback);
        return this;
    }


    public Employee build() {
        return employee;
    }
}
