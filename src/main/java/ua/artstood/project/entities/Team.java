package ua.artstood.project.entities;

import java.util.List;

public class Team {
    private long teamId;
    private List<Employee> employees;

    public Team() {
    }

    public Team(long teamId, List<Employee> employees) {
        this.teamId = teamId;
        this.employees = employees;
    }

    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
        return "Team{" +
                "teamId=" + teamId +
                ", employees=" + employees.size() +
                '}';
    }
}
