package ua.artstood.project.entities;

import ua.artstood.project.entities.enums.DeveloperLevel;
import ua.artstood.project.entities.enums.EnglishLevel;

import java.util.Date;

public class Employee {
    private long employeeId;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String employeeAcronym;
    private String email;
    private String phoneNumber;
    private Date birthdayDate;
    private float experience;
    private Date employmentDate;
    private Project project;
    private DeveloperLevel developerLevel;
    private EnglishLevel englishLevel;
    private String skype;
    private Feedback feedback;

    public long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(long employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getEmployeeAcronym() {
        return employeeAcronym;
    }

    public void setEmployeeAcronym(String employeeAcronym) {
        this.employeeAcronym = employeeAcronym;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getBirthdayDate() {
        return birthdayDate;
    }

    public void setBirthdayDate(Date birthdayDate) {
        this.birthdayDate = birthdayDate;
    }

    public float getExperience() {
        return experience;
    }

    public void setExperience(float experience) {
        this.experience = experience;
    }

    public Date getEmploymentDate() {
        return employmentDate;
    }

    public void setEmploymentDate(Date employmentDate) {
        this.employmentDate = employmentDate;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public DeveloperLevel getDeveloperLevel() {
        return developerLevel;
    }

    public void setDeveloperLevel(DeveloperLevel level) {
        this.developerLevel = level;
    }

    public EnglishLevel getEnglishLevel() {
        return englishLevel;
    }

    public void setEnglishLevel(EnglishLevel level) {
        this.englishLevel = level;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public Feedback getFeedback() {
        return feedback;
    }

    public void setFeedback(Feedback feedback) {
        this.feedback = feedback;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employeeId=" + employeeId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", employeeAcronym='" + employeeAcronym + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber=" + phoneNumber +
                ", birthdayDate=" + birthdayDate +
                ", experience=" + experience +
                ", employmentDate=" + employmentDate +
                ", project=" + project.getProjectName() +
                ", developerLevel=" + developerLevel +
                ", englishLevel=" + englishLevel +
                ", skype='" + skype + '\'' +
                ", feedback=" + feedback +
                '}';
    }
}
